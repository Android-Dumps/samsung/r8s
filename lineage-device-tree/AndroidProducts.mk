#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_r8s.mk

COMMON_LUNCH_CHOICES := \
    lineage_r8s-user \
    lineage_r8s-userdebug \
    lineage_r8s-eng
