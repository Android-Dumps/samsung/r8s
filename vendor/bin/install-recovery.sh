#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/by-name/recovery$(getprop ro.boot.slot_suffix):69009408:8c018f97e8e84678ef43b8bb0720b61ef497d8db; then
  applypatch \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/by-name/boot$(getprop ro.boot.slot_suffix):61865984:201b62d937722e97559e7f23b542eb36f181b1d0 \
          --target EMMC:/dev/block/by-name/recovery$(getprop ro.boot.slot_suffix):69009408:8c018f97e8e84678ef43b8bb0720b61ef497d8db && \
      (/vendor/bin/log -t install_recovery "Installing new recovery image: succeeded" && setprop vendor.ota.recovery.status 200) || \
      (/vendor/bin/log -t install_recovery "Installing new recovery image: failed" && setprop vendor.ota.recovery.status 454)
else
  /vendor/bin/log -t install_recovery "Recovery image already installed" && setprop vendor.ota.recovery.status 200
fi

